package examenISP;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class S2 extends JFrame
{

	private JPanel contentPane;
	private JTextField firstTextField;
	private JTextField secondTextField;

	/* Launch the application. */
	
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		
		{
			public void run() 
			{
				try 
				{
					S2 frame = new S2();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/* Create the frame. */
	
	public S2() 
	{
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		
		firstTextField = new JTextField();
		panel.add(firstTextField);
		firstTextField.setColumns(10);
		
		secondTextField = new JTextField();
		panel.add(secondTextField);
		secondTextField.setColumns(10);
		
		JButton btnNewButton = new JButton("Proceed");
		
		btnNewButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				String string=firstTextField.getText();
				secondTextField.setText(String.valueOf(string.length()));
			}
		});
		
		panel.add(btnNewButton);
	}

}
